package com.manash.demo.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manash.demo.exception.InvalidDataException;
import com.manash.demo.model.Employee;
import com.manash.demo.model.User;
import com.manash.demo.service.EmployeeService;
import com.manash.demo.service.UserService;

@RestController
@RequestMapping("/employee")
public class EmployeeResource {

	@Autowired
	private EmployeeService service;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/getAll", method = RequestMethod.GET, consumes = { "application/json" }, produces = {
			"application/json" })
	public List<Employee> getAllEmployee() {
		return service.getAllEmployee();
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = { "application/json" }, consumes = {
			"application/json" })
	public String addEmployee(@RequestBody Employee emp) {
		String msg = null;
		if (emp.getEname() != null) {
			msg = service.addEmployee(emp);
			return msg;
		} else {
			throw new InvalidDataException();
		}

	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, produces = { "application/json" }, consumes = {
			"application/json" })
	public Employee getEmployeebyId(@PathVariable("id") String empId) {
		return service.getEmployeeById(Integer.valueOf(empId));
	}

	@RequestMapping(value = "/getByName/{name}", method = RequestMethod.GET, produces = { "application/json" })
	public Employee getEmpByName(@PathVariable("name") String name) {
		return service.getEmpByName(name);
	}
	
	@RequestMapping("/check/{name}")
	public String checkAuthenticate(@PathVariable("name") String name) {
		boolean flag=false;
		flag=userService.checkAuthenticate(name);
		return (flag)?"Validate User":"Invalid User!!!!!!!!!";
	}
	
	@RequestMapping("/user/{id}")
	public User getUserById(@PathVariable("id")int id ) {
		return userService.getUser(Integer.valueOf(id));
	}
	
	
}

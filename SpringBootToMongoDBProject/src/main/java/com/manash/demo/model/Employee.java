package com.manash.demo.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import lombok.Data;


@Data
@Document("Employee")
public class Employee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id()
	private int empNo;
	@Field(name = "ENAME",targetType = FieldType.STRING)
	private String ename;
	@Field(name = "Address",targetType = FieldType.OBJECT_ID)
	private Address addr;
	@Field(name = "MOBILENO",targetType = FieldType.INT64)
	private long mobileNo;
	@Field(name = "EMAIL",targetType = FieldType.STRING)
	private String eMail;

}

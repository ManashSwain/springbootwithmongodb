package com.manash.demo.model;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import lombok.Data;

@Data
public class Address implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Field(name = "HouseNo",targetType = FieldType.INT32)
	private int houseNo;
	@Field(name = "Street",targetType = FieldType.STRING)
	private String street;
	@Field(name = "City",targetType = FieldType.STRING)
	private String city;
	@Field(name = "state",targetType = FieldType.STRING)
	private String state;
	@Field(name = "ZipCode",targetType = FieldType.INT32)
	private int zipCode;

}

package com.manash.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import lombok.Data;

@Data
@Document("LOGIN_USER")
public class User {
	
	@Id
	private int id;
	@Field(name = "USER_NAME",targetType = FieldType.STRING)
	private String userName;
	@Field(name = "USER_PWD",targetType = FieldType.STRING)
	private String pwd;

}

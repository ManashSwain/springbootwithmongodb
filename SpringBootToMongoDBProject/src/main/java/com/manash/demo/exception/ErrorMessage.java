package com.manash.demo.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ErrorMessage {
	
	private int errorCode;
	private String errorMsg;
	private String status;

}

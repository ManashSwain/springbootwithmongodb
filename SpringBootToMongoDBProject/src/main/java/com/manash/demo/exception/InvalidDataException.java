package com.manash.demo.exception;

public class InvalidDataException extends RuntimeException {
	
	public InvalidDataException(String msg) {
		super(msg);
	}
	public InvalidDataException() {
		super();
	}

}

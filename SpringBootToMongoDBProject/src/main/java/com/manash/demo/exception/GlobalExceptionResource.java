package com.manash.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionResource {
	
	
	@ExceptionHandler(value=InvalidDataException.class)
	public ResponseEntity<Object> invalidDataException(){
		System.out.println("============================");
		//create error class object
		ErrorMessage msg=new ErrorMessage();
		msg.setErrorCode(400);
		msg.setStatus("failed");
		msg.setErrorMsg("Please Enter valid Employee Data");
		return new ResponseEntity<Object>(msg, HttpStatus.BAD_REQUEST);
	}

}

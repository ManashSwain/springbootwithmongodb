package com.manash.demo.service;

import com.manash.demo.model.User;

public interface UserService {
	
	public boolean checkAuthenticate(String name);

	public User getUser(int id);
}

package com.manash.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manash.demo.model.Employee;
import com.manash.demo.repository.EmployeeRepository;
import com.manash.demo.repository.UserRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository empRepo;

	@Override
	public String addEmployee(Employee emp) {
		Employee inEmp = null;
		inEmp = empRepo.save(emp);
		if (inEmp.getEmpNo() != 0)
			return "Employee data inserted Successfully";
		else
			return "Employee data insertion failled!!!!!!!!!";
	}

	@Override
	public List<Employee> getAllEmployee() {
		return empRepo.findAll();
	}

	@Override
	public Employee getEmployeeById(int empId) {
		Optional<Employee> emp=empRepo.findById(empId);
		return emp.get();
	}
	@Override
	public Employee getEmpByName(String name) {
		return empRepo.getEmpByName(name);
	}
	
	

}

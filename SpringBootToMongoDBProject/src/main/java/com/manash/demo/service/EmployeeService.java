package com.manash.demo.service;

import java.util.List;

import com.manash.demo.model.Employee;

public interface EmployeeService {
	
	public String addEmployee(Employee emp);
	
	public List<Employee> getAllEmployee();
	
	public Employee getEmployeeById(int empId);
	
	public Employee getEmpByName(String name);
	
	
	

}

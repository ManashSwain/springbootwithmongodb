package com.manash.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manash.demo.model.User;
import com.manash.demo.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;

	@Override
	public boolean checkAuthenticate(String name) {
		System.out.println("UserRepository:::"+userRepo);
		User user=userRepo.getEmpByName(name);
		System.out.println("User"+user);
		return user.getId()!=0;
	}

	@Override
	public User getUser(int id) {
		return userRepo.findById(id).get();
	}
	
	

}

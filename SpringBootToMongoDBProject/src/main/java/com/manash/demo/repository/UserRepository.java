package com.manash.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.manash.demo.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, Integer> {
	
	@Query("{'USER_NAME':?0 }")
	public User getEmpByName(String ename);

}

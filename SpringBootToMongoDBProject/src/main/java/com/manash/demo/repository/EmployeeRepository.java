package com.manash.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.manash.demo.model.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, Integer> {

	@Query("{'ename':?0 }")
	public Employee getEmpByName(String ename);

}

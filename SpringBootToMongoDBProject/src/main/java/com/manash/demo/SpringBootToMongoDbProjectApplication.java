package com.manash.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootToMongoDbProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootToMongoDbProjectApplication.class, args);
	}

}
